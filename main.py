import questionary
from os import listdir
from os.path import isfile, join

encryption_methods = [f[:len(f) - 3] for f in listdir('encryption_methods') if isfile(join('encryption_methods', f))]

encryption_method_choice = questionary.select("What encryption method do you want to use?",
                                              list(encryption_methods)).ask()

chosen_module = __import__(f'encryption_methods.{encryption_method_choice}', fromlist=(encryption_method_choice, ''))

function_choice = questionary.select("What do you want to do?",
                                     list(chosen_module.functions.keys())).ask()

index = chosen_module.functions[function_choice]
print(index[0](index[1][0](index[1][1]), index[2][0](index[2][1])))